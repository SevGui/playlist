<?php

/**
 * Description of MyDao
 * permet de se connecter a la bdd
 * et de traiter les requetes SQL
 * @author sev
 */
class MyDao {

    private $datasource;
    private $user;
    private $password;
    private $conn;

    function __construct() {
        $conf = parse_ini_file("./config/conf.ini", TRUE);
        $settings = $conf['settings']['env'];
        $host = $conf[$settings]['host'];
        $dbname = $conf[$settings]['db_name'];
        $this->user = $conf[$settings]['user'];
        $this->password = $conf[$settings]['password'];
        $this->datasource = "mysql:host=$host;dbname=$dbname";
        $this->conn = new PDO($this->datasource, $this->user, $this->password, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }

    //Methods for videos
    /*
     * getVideo
     * Return the list of all videos 
     */
    public function getVideo() {
        $query = "Select * from video ORDER BY id_video";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute();
        if ($sth->rowCount() === 0) {
            $resu = array();
        } else {
            $resu = $sth->fetchAll();
        }
        return $resu;
    }

    /*
     * getVideoOrderPlaylist
     * Return the list of all videos from a playlist (ordered by position)
     */

    public function getVideoOrderPlaylist() {
        $query = "Select * from video ORDER BY id_playlistvideo, id_video";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute();
        if ($sth->rowCount() === 0) {
            $resu = array();
        } else {
            $resu = $sth->fetchAll();
        }
        return $resu;
    }
    
    /*
     * getVideoByPlaylist
     * Return the list of all videos from a playlist (ordered by position)
     */

    public function getVideoByPlaylist($idPlaylist) {
        $query = "SELECT * FROM video INNER JOIN playlist ON "
                . "id_playlistvideo = id_playlist WHERE id_playlistvideo =?";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($idPlaylist));
        if ($sth->rowCount() === 0) {
            $resu = array();
        } else {
            $resu = $sth->fetchAll();
        }
        return $resu;
    }

    /*
     * updateVideo
     * Add a video in a playlist
     * Delete a video from a playlist
     */

    public function updateVideo(videoClass $myVideo) {
        $query = "UPDATE video SET id_playlistvideo=:playlist "
                . "WHERE id_video=:id";
        $sth = $this->conn->prepare($query);
        $sth->execute(array(':id' => $myVideo->getIdVideo(),
            ':playlist' => $myVideo->getIdPlaylistVideo()));
        return TRUE;
    }

    //Methods for playlist
    /*
     * getPlaylist
     * Return the list of all playlists 
     */
    public function getPlaylist() {
        $query = "Select * from playlist ORDER BY id_playlist";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute();
        if ($sth->rowCount() === 0) {
            $resu = array();
        } else {
            $resu = $sth->fetchAll();
        }
        return $resu;
    }

    /*
     * insertPlaylist
     * Create a playlist
     */

    public function insertPlaylist(playlistClass $myPlaylist) {
        $query = "INSERT INTO playlist SET id_playlist=:id,"
                . "name=:name";
        $sth = $this->conn->prepare($query);
        $sth->execute(array(':id' => $myPlaylist->getIdPlaylist(),
            ':name' => $myPlaylist->getName()));
        return TRUE;
    }

    /*
     * getPlaylistByID
     * Show informations about the playlist
     */

    public function getPlaylistByID($id) {
        $query = "SELECT * from playlist WHERE id_playlist=?";
        $sth = $this->conn->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(array($id));
        if ($sth->rowCount() === 0) {
            $resu = NULL;
        } else {
            $resu = $sth->fetch();
        }
        return $resu;
    }

    /*
     * updatePlaylist
     * Update informations about the playlist
     */

    public function updatePlaylist(playlistClass $myPlaylist) {
        $query = "UPDATE playlist SET name=:name "
                . "WHERE id_playlist=:id";
        $sth = $this->conn->prepare($query);
        $sth->execute(array(':name' => $myPlaylist->getName(),
            ':id' => $myPlaylist->getIdPlaylist()));
        return TRUE;
    }

    /*
     * deletePlaylist
     * Delete the playlist
     */

    public function deletePlaylist($id) {
        $query = "DELETE FROM playlist WHERE id_playlist=:id";
        $sth = $this->conn->prepare($query);
        $sth->execute(array(':id' => $id));
        return TRUE;
    }

}
