<?php

include_once './dao/MyDao.php';
include_once './entity/pagedataClass.php';
include_once './entity/pagepostClass.php';
include_once './entity/playlistClass.php';
include_once './entity/videoClass.php';

$myret['action'] = "";
$pageData = new PageData();
$post = PagePost::allPost();

$action = array('create', 'show', 'update', 'updating', 'delete', 'addvideo',
    'adding', 'deletevideo', 'deleting');

if ($post != NULL) {
    foreach ($post as $key => $value) {
        foreach ($action as $actionValue) {
            if (strpos($key, $actionValue) !== FALSE) {
                $myret['action'] = $actionValue;
                $myret['index'] = filter_var($key, FILTER_SANITIZE_NUMBER_INT);
            }
        }
    }
}

switch ($myret['action']) {
    case 'create':
        $myPlaylist = new playlistClass(NULL, PagePost::myPOST('namePlaylist'));
        $dao = new MyDao();
        $dataPlaylist = $dao->insertPlaylist($myPlaylist);
        break;
    case 'show':
        $pageData->content .= include_once "views/onePlaylist.php";
        break;
    case 'update':
        $pageData->content .= include_once "views/updatePlaylist.php";
        break;
    case 'updating':
        $myPlaylist = new playlistClass($myret['index'], PagePost::myPOST('namePlaylist'));
        $dao = new MyDao();
        $dataPlaylist = $dao->updatePlaylist($myPlaylist);
        break;
    case 'delete':
        $dao = new MyDao();
        $dataPlaylist = $dao->deletePlaylist($myret['index']);
        break;
    case 'addvideo':
        $pageData->content .= include_once "views/updateVideoPlaylist.php";
        break;
    case 'adding':
        $myVideo = new videoClass(PagePost::myPOST('id_video'), NULL, NULL, $myret['index']);
        $dao = new MyDao();
        $dataVideo = $dao->updateVideo($myVideo);

        break;
    case 'deletevideo':
        $pageData->content .= include_once "views/deleteVideoPlaylist.php";
        break;
    case 'deleting':
        $myVideo = new videoClass(PagePost::myPOST('id_video'), NULL, NULL, NULL);
        $dao = new MyDao();
        $dataVideo = $dao->updateVideo($myVideo);

        break;
    default:
        break;
}

$pageData->content .= include_once "views/createPlaylist.php";
$pageData->content .= include_once "views/allPlaylistWithVideo.php";
$pageData->content .= include_once "views/allVideo.php";
$pageData->content .= include_once "views/allPlaylist.php";
$page = include_once "templates/page.php";

echo $page;
?>

