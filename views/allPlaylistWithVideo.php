<?php

include_once './dao/MyDao.php';

$dao = new MyDao();
$dataVideo = $dao->getVideoOrderPlaylist();

$content = <<<EOT
<div>
        video with playlist :<br>
EOT;

foreach ($dataVideo as $video) {
    $content .= <<<EOT
    <ul>
        <li>id : {$video['id_video']}</li>
        <li>title : {$video['title']} from playlist : {$video['id_playlistvideo']}</li>
        <li>thumbnail : {$video['thumbnail']}</li>
    </ul>
EOT;
}
$content .= <<<EOT
</div>
EOT;
return $content;
?>


