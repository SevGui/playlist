<?php
include_once './dao/MyDao.php';

$dao = new MyDao();
$dataVideo = $dao->getVideo();
$content = <<<EOT
<div>
    video :<br>
EOT;

foreach ($dataVideo as $video){
    $content .= <<<EOT
    <ul>
        <li>id : {$video['id_video']}</li>
        <li>title : {$video['title']}</li>
        <li>thumbnail : {$video['thumbnail']}</li>
    </ul>
EOT;
}
$content .= <<<EOT
</div>
EOT;
return $content;
?>


