<?php

/**
 * Description of pagepostClass
 *
 * @author sev
 */
class PagePost {

    public static function myPOST($myVariable) {
        /*
         * filter_input returns: 
         * Value of the requested variable on success, 
         * FALSE if the filter fails, or 
         * NULL if the variable_name variable is not set
         */
        return (NULL == (filter_input(INPUT_POST, $myVariable))) ?
                FALSE :
                filter_input(INPUT_POST, $myVariable, FILTER_SANITIZE_STRING);
    }
    
    public static function allPost() {
        return filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    }

}
