<?php

/**
 * Description of playlist
 * Correspond aux colonnes de la table playlist
 * @author sev
 */
class playlistClass {
    //Attributs
    private $id_playlist;
    private $name;
    
    //Methodes
    
    function __construct($id_playlist, $name) {
        $this->id_playlist = $id_playlist;
        $this->name = $name;
    }

    function getIdPlaylist() {
        return $this->id_playlist;
    }

    function getName() {
        return $this->name;
    }

    function setIdPlaylist($id_playlist) {
        $this->id_playlist = $id_playlist;
    }

    function setName($name) {
        $this->name = $name;
    }


    
}
