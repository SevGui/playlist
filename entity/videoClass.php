<?php

/**
 * Description of videoClass
 *
 * @author sev
 */
class videoClass {
    //Attributs
    private $id_video;
    private $title;
    private $thumbnail;
    private $id_playlistvideo;
    
    //Methodes
    function __construct($id_video, $title, $thumbnail, $id_playlistvideo) {
        $this->id_video = $id_video;
        $this->title = $title;
        $this->thumbnail = $thumbnail;
        $this->id_playlistvideo = $id_playlistvideo;
    }
    function getIdVideo() {
        return $this->id_video;
    }

    function getTitle() {
        return $this->title;
    }

    function getThumbnail() {
        return $this->thumbnail;
    }

    function getIdPlaylistVideo() {
        return $this->id_playlistvideo;
    }

    function setIdVideo($id_video) {
        $this->id_video = $id_video;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setThumbnail($thumbnail) {
        $this->thumbnail = $thumbnail;
    }

    function setIdPlaylistVideo($id_playlistvideo) {
        $this->id_playlistvideo = $id_playlistvideo;
    }


}
